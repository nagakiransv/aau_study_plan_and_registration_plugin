class CreateAauSubjectsFedenaSubjects < ActiveRecord::Migration
  def self.up
    create_table :aau_subjects_fedena_subjects do |t|
    	t.integer "aau_subject_id"
    	t.integer "fedena_subject_id"
    	t.integer "batch_id"
    	t.integer 'elective_group_id'
    	t.boolean  "is_deleted", :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :aau_subjects_fedena_subjects
  end
end
