class CreateAauSubjectsStudyPlans < ActiveRecord::Migration
  def self.up
    create_table :aau_subjects_study_plans do |t|
    	t.integer "aau_subject_id"
    	t.integer "study_plan_id"
    	t.boolean  "equivalence",      :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :aau_subjects_study_plans
  end
end
