class CreateStudyPlans < ActiveRecord::Migration
  def self.up
    create_table :study_plans do |t|
    	t.decimal "credit_hours"
      	t.string "plan_issue"
      	t.integer "user_id"
      	t.boolean  "approved",      :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :study_plans
  end
end
