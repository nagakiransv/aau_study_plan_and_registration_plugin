class AddCategoryToAauSubjects < ActiveRecord::Migration
  def self.up
  	add_column :aau_subjects, :study_plan_category_id, :integer
    add_column :aau_subjects, :sub_category_id, :integer
  end

  def self.down
  	remove_column :aau_subjects, :study_plan_category_id
  	remove_column :aau_subjects, :sub_category_id
  end
end
