class CreateAauSubjects < ActiveRecord::Migration
  def self.up
    create_table :aau_subjects do |t|
    t.string   "aau_subject_name"
    t.integer   "aau_subject_number"
    t.string   "aau_subject_code"
    t.string   "aau_subject_description"
    t.integer  "subject_type_id"
    t.boolean  "is_deleted",                                        :default => false
    t.decimal  "aau_credit_hours",       :precision => 15, :scale => 2
      t.timestamps
    end
  end

  def self.down
    drop_table :aau_subjects
  end
end



