class CreateSubCategories < ActiveRecord::Migration
  def self.up
    create_table :sub_categories do |t|
      t.references :study_plan_category
      t.string :name
      t.string :code
      t.text :description
      t.string :credit_hours

      t.timestamps
    end
  end

  def self.down
    drop_table :sub_categories
  end
end
