class CreatePreparationCourses < ActiveRecord::Migration
  def self.up
    create_table :preparation_courses do |t|
	    t.string   "name"
	    t.integer   "number"
	    t.string   "code"
	    t.string   "description"
	    t.boolean  "is_deleted",                                        :default => false
	    t.decimal  "credit_hours",       :precision => 15, :scale => 2
      t.timestamps
    end
  end

  def self.down
    drop_table :preparation_courses
  end
end
