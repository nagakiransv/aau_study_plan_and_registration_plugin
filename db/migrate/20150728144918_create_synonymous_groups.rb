class CreateSynonymousGroups < ActiveRecord::Migration
  def self.up
    create_table :synonymous_groups do |t|
      t.string :name
      t.string :code
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :synonymous_groups
  end
end
