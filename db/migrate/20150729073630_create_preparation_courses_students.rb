class CreatePreparationCoursesStudents < ActiveRecord::Migration
  def self.up
    create_table :preparation_courses_students do |t|
    	t.integer :preparation_course_id
      	t.text :student_id
      t.timestamps
    end
  end

  def self.down
    drop_table :preparation_courses_students
  end
end
