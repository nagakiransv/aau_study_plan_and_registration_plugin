class CreateBatchesStudyPlans < ActiveRecord::Migration
  def self.up
    create_table :batches_study_plans do |t|
    	t.integer "study_plan_id"
    	t.integer "batch_id"
      t.timestamps
    end
  end

  def self.down
    drop_table :batches_study_plans
  end
end
