class CreateStudyPlanCategories < ActiveRecord::Migration
  def self.up
    create_table :study_plan_categories do |t|
      t.string :name
      t.string :code
      t.text :description
      t.string :credit_hours

      t.timestamps
    end
  end

  def self.down
    drop_table :study_plan_categories
  end
end
