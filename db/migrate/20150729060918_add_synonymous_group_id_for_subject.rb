class AddSynonymousGroupIdForSubject < ActiveRecord::Migration
  def self.up
  add_column :aau_subjects, :synonymous_group_id , :integer, :references => "synonymous_groups"
  end

  def self.down
  remove_column :aau_subjects, :synonymous_group_id 
  end
end
