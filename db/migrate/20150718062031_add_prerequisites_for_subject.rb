class AddPrerequisitesForSubject < ActiveRecord::Migration
  def self.up
  	add_column :aau_subjects, :prerequisite, :text  
  end

  def self.down
  	remove_column :aau_subjects, :prerequisite
  end
end
