class CreateStudyPlanCategoriesStudyPlans < ActiveRecord::Migration
  def self.up
    create_table :study_plan_categories_study_plans do |t|
    	t.integer "study_plan_category_id"
    	t.integer "study_plan_id"
      t.timestamps
    end
  end

  def self.down
    drop_table :study_plan_categories_study_plans
  end
end
