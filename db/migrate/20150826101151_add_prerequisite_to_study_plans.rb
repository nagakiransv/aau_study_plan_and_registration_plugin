class AddPrerequisiteToStudyPlans < ActiveRecord::Migration
  def self.up
  	add_column :aau_subjects_study_plans, :prerequisite, :text
  end

  def self.down
  	add_column :aau_subjects_study_plans, :prerequisite
  end
end
