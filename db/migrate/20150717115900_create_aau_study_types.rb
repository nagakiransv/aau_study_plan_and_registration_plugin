class CreateAauStudyTypes < ActiveRecord::Migration
  def self.up
    create_table :aau_study_types do |t|
      t.string :name
      t.text :description
      t.string :code

      t.timestamps
    end
  end

  def self.down
    drop_table :aau_study_types
  end
end