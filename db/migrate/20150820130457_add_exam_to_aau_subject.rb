class AddExamToAauSubject < ActiveRecord::Migration
  def self.up
  	add_column :aau_subjects, :no_exam, :boolean ,:default => false
  	add_column :aau_subjects, :max_weekly_classes, :integer
  end

  def self.down
  	remove_column :aau_subjects, :no_exam
  	remove_column :aau_subjects, :max_weekly_classes
  end
end
