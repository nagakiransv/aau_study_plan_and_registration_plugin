class AddBatchIdToPreparationCoursesStudents < ActiveRecord::Migration
  def self.up
    add_column :preparation_courses_students, :batch_id, :integer
  end

  def self.down
    remove_column :preparation_courses_students, :batch_id
  end
end
