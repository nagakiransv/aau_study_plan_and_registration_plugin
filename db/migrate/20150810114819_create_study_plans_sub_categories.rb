class CreateStudyPlansSubCategories < ActiveRecord::Migration
  def self.up
    create_table :study_plans_sub_categories do |t|
    	t.integer "sub_category_id"
    	t.integer "study_plan_id"
      t.timestamps
    end
  end

  def self.down
    drop_table :study_plans_sub_categories
  end
end
