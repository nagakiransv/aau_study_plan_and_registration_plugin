Gretel::Crumbs.layout do

  ########################################
  #aau_study_type module
  ########################################

	crumb :aau_study_types_edit_study_type do|aau|
		link I18n.t('edit_study_type'), {:controller=>"aau_study_types", :action=>"edit_study_type", :id=>aau.id}
		parent :aau_study_types_new_study_type
	end

	crumb :aau_study_types_new_study_type do
		link I18n.t('study_types'), {:controller=>"aau_study_types", :action=>"new_study_type"}
		parent :aau_home_index
	end

	crumb :aau_home_index do
		link I18n.t('aau_settings'), {:controller=>"aau_home", :action=>"index"}
		parent :configuration_index
	end
	
	crumb :configuration_index do
		link I18n.t('config'), {:controller=>"configuration", :action=>"index"}
	end

  ########################################
  #study_plan_category module
  ########################################

    crumb :study_plan_categories_edit_study_plan_category do|study_plan_category| 
		link I18n.t('edit_study_plan_category'), {:controller=>"study_plan_categories", :action=>"edit_study_plan_category", :id=>study_plan_category.id}
		parent :study_plan_categories_add_study_plan_category
	end

	crumb :study_plan_categories_show do |study_plan_category|
		link I18n.t('show'), {:controller=>"study_plan_categories", :action=>"show",:id=>study_plan_category.id}
		parent :study_plan_categories_add_study_plan_category
	end

	crumb :study_plan_categories_add_study_plan_category do
		link I18n.t('study_plan_categories'), {:controller=>"study_plan_categories", :action=>"add_study_plan_category"}
		parent :aau_home_index
   end

   	crumb :study_plan_categories_add_electives do |study_plan_category|
		link I18n.t('add_new_electives'), {:controller=>"study_plan_categories", :action=>"add_electives"}
		parent :study_plan_categories_add_study_plan_category
   end

    crumb :study_plan_categories_edit_electives do |study_plan_category|
		link I18n.t('edit_electives'), {:controller=>"study_plan_categories", :action=>"edit_electives", :id=>study_plan_category.id}
		parent :study_plan_categories_add_study_plan_category
   end
   



  ########################################
  #aau_subject module
  ########################################


  crumb :aau_subjects_add_aau_subject do 
		link I18n.t('add_aau_subject'), {:controller=>"aau_subjects", :action=>"add_aau_subject"}
		parent :aau_home_index
	end

	 crumb :aau_subjects_edit_aau_subject do |edit_aau_subject|
		link I18n.t('edit_aau_subject'), {:controller=>"aau_subjects", :action=>"edit_aau_subject", :id=>edit_aau_subject.id}
		parent :aau_subjects_add_aau_subject
	end

	crumb :aau_subjects_prerequisite_subjects do 
		link I18n.t('aau_subject_prerequisite'), {:controller=>"aau_subjects", :action=>"prerequisite_subjects"}
		parent :aau_home_index
	end

	crumb :subject_types_add_subject_type do 
		link I18n.t('add_subject_types'), {:controller=>"subject_types", :action=>"add_subject_type"}
		parent :aau_home_index
	end

	crumb :subject_types_edit_subject_type do |edit_subject_type|
		link I18n.t('edit_subject_types'), {:controller=>"subject_types", :action=>"edit_subject_type", :id=>edit_subject_type.id}
		parent :subject_types_add_subject_type 
	end

  ########################################
  #preparation_courses module
  ########################################

    crumb :preparation_courses_index do 
		link I18n.t('preparation_courses'), {:controller=>"preparation_courses", :action=>"index"}
		parent :aau_home_index
	end

	crumb :preparation_courses_new do 
		link I18n.t('new'), {:controller=>"preparation_courses", :action=>"new"}
		parent :preparation_courses_index
	end

	 crumb :preparation_courses_edit do |edit|
		link I18n.t('edit'), {:controller=>"preparation_courses", :action=>"edit", :id=>edit.id}
		parent :preparation_courses_index
	end

	crumb :preparation_courses_assign_preparation_courses do |assign|
		link I18n.t('assign_students'), {:controller=>"preparation_courses", :action=>"assign_preparation_courses", :id=>assign.id}
		parent :preparation_courses_index
	end
  ########################################
  #synonymous_groups
  ########################################

    crumb :synonymous_groups_edit_synonymous_group do|synonymous_group| 
		link I18n.t('edit_synonymous_group'), {:controller=>"synonymous_groups", :action=>"edit_synonymous_group", :id=>synonymous_group.id}
		parent :synonymous_groups_new_synonymous_group
	end

	crumb :synonymous_groups_show_subjects do |synonymous_group|
		link I18n.t('show_subjects'), {:controller=>"synonymous_groups", :action=>"show_subjects", :id=>synonymous_group.id}
		parent :synonymous_groups_new_synonymous_group
	end

	crumb :synonymous_groups_new_synonymous_group do
		link I18n.t('synonymous_groups'), {:controller=>"synonymous_groups", :action=>"new_synonymous_group"}
		parent :aau_home_index
   end

   ########################################
   #study_plan_category module
   ########################################

   crumb :study_plans_index do
		link I18n.t('study_plans'), {:controller=>"study_plans", :action=>"index"}
		parent :aau_home_index
   end

   crumb :study_plans_new do
		link I18n.t('new'), {:controller=>"study_plans", :action=>"new"}
		parent :study_plans_index
   end

   crumb :study_plans_edit do|edit| 
		link I18n.t('edit_study_plan'), {:controller=>"study_plans", :action=>"edit", :id=>edit.id}
		parent :study_plans_index
	end

	crumb :study_plans_show do |show|
		link I18n.t('study_plan'), {:controller=>"study_plans", :action=>"show", :id=>show.id}
		parent :study_plans_index
	end

	crumb :study_plans_assign_study_plan do |assign|
		link I18n.t('assign_study_plan'), {:controller=>"study_plans", :action=>"assign_study_plan", :id=>assign.id}
		parent :study_plans_index
	end

end