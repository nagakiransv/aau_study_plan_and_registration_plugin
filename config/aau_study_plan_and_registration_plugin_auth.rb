authorization do

  role :admin do
    has_permission_on [:aau_subjects],
      :to => [
        :add_aau_subject,
        :edit_aau_subject,
        :delete_aau_subject  
    ]
    has_permission_on [:subject_types],
      :to => [
        :add_subject_type,
        :edit_subject_type,
        :delete_subject_type  
    ]
     has_permission_on [:aau_study_types],
      :to => [
        :new_study_type,
        :edit_study_type,
        :delete_study_type  
    ]
     has_permission_on [:study_plan_categories],
      :to => [
        :add_study_plan_category,
        :edit_study_plan_category,
        :delete_study_plan_category,
        :show  
    ]
     has_permission_on [:configuration],
      :to => [
        :index
      ]
      has_permission_on [:aau_home],
      :to => [
        :index
      ]
    has_permission_on [:preparation_courses],
      :to => [
        :index,:new,:edit,:destroy,:assign_preparation_courses,:list_students 
    ]
    has_permission_on [:synonymous_groups],
      :to => [
        :new_synonymous_group,:edit_synonymous_group,:delete_synonymous_group,:show_subjects,:list_plan_category,:list_category
    ]
    has_permission_on [:study_plans],
      :to => [
        :index,:new,:edit,:destroy,:show,:assign_study_plan
      ]
  end
end
