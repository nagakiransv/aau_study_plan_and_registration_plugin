ActionController::Routing::Routes.draw do |map|
  
  # support
  map.resources :subject_types, 
   :except=>[:index, :show, :new, :create, :edit, :update, :destroy],
    :collection => {
    },
    :member => {
    }
map.resources :aau_study_types, 
   :except=>[:index, :show, :new, :create, :edit, :update, :destroy],
    :collection => {
    },
    :member => {
    	:new_study_type=>[:get,:post],
    	:edit_study_type=>[:get,:post],
    	:delete_study_type=>[:get,:post]
    }
map.resources :aau_home

map.resources :study_plan_categories, 
   :except=>[:index, :show, :new, :create, :edit, :update, :destroy],
    :collection => {
    },
    :member => {
      :add_study_plan_category=>[:get,:post],
      :edit_study_plan_category=>[:get,:post],
      :delete_study_plan_category=>[:get,:post]
    }

map.resources :preparation_courses, 
   :except=>[:index, :show, :new, :edit, :destroy],
    :collection => {
    },
    :member => {
      :assign_preparation_courses=>[:get,:post],
      :list_students=>[:get,:post]
    }
map.resources :study_plans, 
   :except=>[:index, :show, :new, :edit, :destroy],
    :collection => {
    },
    :member => {
      :assign_study_plan=>[:get,:post]
    }

 end
