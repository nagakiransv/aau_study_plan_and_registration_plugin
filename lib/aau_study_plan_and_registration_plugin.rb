# AauStudyPlanAndRegistrationPlugin
require 'dispatcher'
require 'overrides/aau_configurations_controller'

module AauStudyPlanAndRegistrationPlugin
	def self.attach_overrides
      Dispatcher.to_prepare  do
      	::ConfigurationController.instance_eval { include AauConfigurationsController }
      end
    end


 end
