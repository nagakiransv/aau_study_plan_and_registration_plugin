# desc "Explaining what the task does"
# task :aau_study_plan_and_registration_plugin do
#   # Task goes here
# end

namespace :aau_study_plan_and_registration_plugin do
 desc "Install Aau Study Plan And Registration Plugin"
 task :install do
 	system "rsync --exclude=.svn -ruv vendor/plugins/aau_study_plan_and_registration_plugin/public ."
 	system "rsync -ruv --exclude=.svn vendor/plugins/aau_study_plan_and_registration_plugin/db/migrate db"
 end
end

