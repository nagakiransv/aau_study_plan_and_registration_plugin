# Include hook code here
require 'fastercsv'
require 'translator'
require File.join(File.dirname(__FILE__), "lib", "aau_study_plan_and_registration_plugin")
require File.join(File.dirname(__FILE__), "config", "breadcrumbs")

FedenaPlugin.register = {
 :name=>"aau_study_plan_and_registration_plugin",
 :description=>"AauStudyPlanAndRegistrationPlugin",
 :auth_file=>"config/aau_study_plan_and_registration_plugin_auth.rb"
}

Dir[File.join("#{File.dirname(__FILE__)}/config/locales/*.yml")].each do |locale|
 I18n.load_path.unshift(locale)
end

AauStudyPlanAndRegistrationPlugin.attach_overrides

if RAILS_ENV == 'development'
 ActiveSupport::Dependencies.load_once_paths.\
 reject!{|x| x =~ /^#{Regexp.escape(File.dirname(__FILE__))}/}
end
