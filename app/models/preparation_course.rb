class PreparationCourse < ActiveRecord::Base

	named_scope :active,{ :conditions => { :is_deleted => false } }
	named_scope :inactive,{ :conditions => { :is_deleted => true } }
	validates_presence_of :name
	validates_uniqueness_of :name
  has_many :preparation_courses_students, :dependent => :destroy
  has_many :students, :through => :preparation_courses_students
end
