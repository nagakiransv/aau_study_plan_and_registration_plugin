class StudyPlan < ActiveRecord::Base
	validates_presence_of :plan_issue
	validates_uniqueness_of :plan_issue

	validates_numericality_of :credit_hours


	has_many :aau_subjects_study_plans, :dependent => :destroy
  	has_many :aau_subjects, :through => :aau_subjects_study_plans

  	has_many :study_plan_categories_study_plans, :dependent => :destroy
  	has_many :study_plan_categories, :through => :study_plan_categories_study_plans

  	has_many :study_plans_sub_categories, :dependent => :destroy
  	has_many :sub_categories, :through => :study_plans_sub_categories

  	has_many :batches_study_plans, :dependent => :destroy
  	has_many :batches, :through => :batches_study_plans
end
