class AauStudyType < ActiveRecord::Base
	validates_presence_of :name ,:message => "is required."
	validates_uniqueness_of :name, :message => "is already Given to other Study Type"
	validates_presence_of :code, :message => "is required."
	validates_uniqueness_of :code, :message => "is already Given to other Study Type"
end
