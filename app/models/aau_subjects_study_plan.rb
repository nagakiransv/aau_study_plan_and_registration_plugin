class AauSubjectsStudyPlan < ActiveRecord::Base
	validates_presence_of :aau_subject_id
	validates_presence_of :study_plan_id
	serialize :prerequisite, Array
	belongs_to :aau_subjects, :foreign_key => 'aau_subject_id'
  	belongs_to :study_plans, :foreign_key => 'study_plan_id'
end