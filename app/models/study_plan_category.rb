class StudyPlanCategory < ActiveRecord::Base
	has_many :sub_categories
	has_many :aau_subjects, :dependent=>:destroy
	validates_presence_of :name
  validates_format_of :name, :with => /^[^0-9]/
  validates_presence_of :code
  validates_uniqueness_of :code
  validates_presence_of :credit_hours
  validates_numericality_of :credit_hours,:greater_than_or_equal_to => 6

   has_many :study_plan_categories_study_plans, :dependent => :destroy
   has_many :study_plans, :through => :study_plan_categories_study_plans

end


