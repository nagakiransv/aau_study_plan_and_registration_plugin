class SubCategory < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :code
  belongs_to :study_plan_category
  has_many :aau_subjects, :dependent=>:destroy
  has_many :study_plans_sub_categories, :dependent => :destroy
  has_many :study_plans, :through => :study_plans_sub_categories
  named_scope :for_plan, lambda { |b| { :conditions => { :study_plan_category_id => b.to_i} } }
end
