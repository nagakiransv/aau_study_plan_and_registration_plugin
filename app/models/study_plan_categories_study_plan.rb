class StudyPlanCategoriesStudyPlan < ActiveRecord::Base
	validates_presence_of :study_plan_category_id
	validates_presence_of :study_plan_id
	belongs_to :study_plan_categories, :foreign_key => 'study_plan_category_id'
  	belongs_to :study_plans, :foreign_key => 'study_plan_id'
end
