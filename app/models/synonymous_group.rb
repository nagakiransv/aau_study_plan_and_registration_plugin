class SynonymousGroup < ActiveRecord::Base
	has_many :aau_subjects
    validates_presence_of :name
	validates_presence_of :code
end
