class PreparationCoursesStudent < ActiveRecord::Base
	belongs_to :preparation_courses, :foreign_key => 'preparation_course_id'
  	belongs_to :students, :foreign_key => 'student_id'
	serialize :student_id, Array
end
