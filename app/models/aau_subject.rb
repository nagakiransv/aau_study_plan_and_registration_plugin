class AauSubject < ActiveRecord::Base

	named_scope :active,{ :conditions => { :is_deleted => false } }
	named_scope :inactive,{ :conditions => { :is_deleted => true } }
	belongs_to :subject_type 
	belongs_to :sub_category 
	belongs_to :study_plan_category
	belongs_to :synonymous_group
	validates_presence_of :subject_type_id
	validates_presence_of :study_plan_category_id
	validates_presence_of :sub_category_id
	validates_presence_of :aau_subject_code
	validates_uniqueness_of :aau_subject_code
	validates_presence_of :aau_subject_name
	validates_presence_of :aau_credit_hours
	validates_numericality_of :aau_credit_hours
	validates_numericality_of :max_weekly_classes
	serialize :prerequisite, Array

	has_many :aau_subjects_study_plans, :dependent => :destroy
  	has_many :study_plans, :through => :aau_subjects_study_plans
end
