class SubjectType < ActiveRecord::Base
   has_many :aau_subjects, :dependent=>:destroy
   validates_format_of :name, :with => /[^\b^\d]/,:message=>"should contain only letters"
end