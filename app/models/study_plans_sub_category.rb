class StudyPlansSubCategory < ActiveRecord::Base
	validates_presence_of :sub_category_id
	validates_presence_of :study_plan_id
	belongs_to :sub_categories, :foreign_key => 'sub_category_id'
  	belongs_to :study_plans, :foreign_key => 'study_plan_id'
end
