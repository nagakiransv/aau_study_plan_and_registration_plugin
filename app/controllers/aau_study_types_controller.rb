class AauStudyTypesController < ApplicationController

	def new_study_type
    @aau_study_types = AauStudyType.find(:all,:order => "name asc")
    @study_type = AauStudyType.new(params[:aau_study_type])
    if request.post? and @study_type.save
      flash[:notice] =  t('add_study_type_success')
      redirect_to :action => "new_study_type" 
    end
  end
  def edit_study_type
    @aau_study_type = AauStudyType.find(params[:id])
    if request.post? and @aau_study_type.update_attributes(params[:aau_study_type])
      flash[:notice] = "#{t('updated_study_type_success')}"
      redirect_to :action => "new_study_type"
    end
  end

  def delete_study_type
    @aau_study_type = AauStudyType.find(params[:id])
    @aau_study_type.destroy
    flash[:notice]="#{t('deleted_successfully')}"
    redirect_to :action => "new_study_type"
  end

end
