class AauSubjectsController < ApplicationController

  def add_aau_subject
    @aau_subjects = AauSubject.find(:all,:order => "aau_subject_name asc",:conditions => ["is_deleted = ?", false]).paginate(:page => params[:page], :per_page => 10)
    @aau_subject = AauSubject.new(params[:aau_subject])
    @subject_type = SubjectType.all
    @study_plan_categories =StudyPlanCategory.find(:all)
    @sub_categories =SubCategory.find(:all)
    @synonymous_groups = SynonymousGroup.find(:all)
    if request.post? and @aau_subject.save
      flash[:notice] =  t('add_subject_success')
      redirect_to :action => "add_aau_subject"
    end
  end
  
  def edit_aau_subject
      @aau_subject = AauSubject.find(params[:id])
      @subject_type = SubjectType.all
      @study_plan_categories =StudyPlanCategory.find(:all)
      @sub_categories =SubCategory.find(:all)
      @synonymous_groups = SynonymousGroup.find(:all)
      if request.post? and @aau_subject.update_attributes(params[:aau_subject])
        if(@fedena_subject = AauSubjectsFedenaSubject.find_by_aau_subject_id(@aau_subject.id))
          @fedena_subjects = AauSubjectsFedenaSubject.find_all_by_aau_subject_id(@aau_subject.id)
          @subject = []
          @fedena_subjects.each do |i|
            @subject << i.fedena_subject_id
          end
          @subject.each do |i|
            @sub_find = Subject.find(i)
            if @aau_subject.sub_category.name == "Mandatory"
               @sub_find.update_attributes(:name => @aau_subject.aau_subject_name,:code => @aau_subject.aau_subject_code,:max_weekly_classes => @aau_subject.max_weekly_classes, :no_exams => @aau_subject.no_exam,:elective_group_id => nil)
            else
              @batchs = Batch.find(@sub_find.batch_id)
                if @batchs.elective_groups.empty?
                  @elective_group = ElectiveGroup.new(:batch_id => @batchs.id, :name => "Optional")
                  @elective_group.save
                  @sub_find.update_attributes(:name => @aau_subject.aau_subject_name,:code => @aau_subject.aau_subject_code,:max_weekly_classes => @aau_subject.max_weekly_classes, :no_exams => @aau_subject.no_exam,:elective_group_id => @elective_group.id)
                  @aau_fedena = AauSubjectsFedenaSubject.find_by_batch_id_and_fedena_subject_id(@batchs.id,i)
                  @aau_fedena.update_attribute(:elective_group_id,@batchs.elective_groups.find_by_name("Optional").id)   
                else
                  @sub_find.update_attributes(:name => @aau_subject.aau_subject_name,:code => @aau_subject.aau_subject_code,:max_weekly_classes => @aau_subject.max_weekly_classes, :no_exams => @aau_subject.no_exam,:elective_group_id => @batchs.elective_groups.find_by_name("Optional").id)
                end
            end
          end
        end
        flash[:notice] = "#{t('success')}"
        redirect_to :action => "add_aau_subject"
      end
  end

#Delete within the Subjects
def delete_aau_subject
 @aau_subject = AauSubject.find(params[:id])
 if !@aau_subject.aau_subjects_study_plans.present?
    # @aau_subject.destroy
    @aau_subject.update_attribute(:is_deleted, true)
    flash[:notice]="#{t('deleted_successfully')}"
    redirect_to :action => "add_aau_subject"
 else
  flash[:notice] = "#{t('it_has_assigned_to_study_plan_so_you_cant_delete_this_study_plan')}"
  redirect_to :action => "add_aau_subject"
 end 
end


def list_plans
 @study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
 @sub_category = @study_plan_category.sub_categories
 render :update do |page|
  page.replace_html 'list-category-batch', :partial => 'list_plans'
end
end

# List Of Deleted Subjects 
def deleted_subjects
  @aau_subjects = AauSubject.find(:all, :conditions => ["is_deleted = ?", true]).paginate(:page => params[:page], :per_page => 10)
end

#Delete Completely Aau Subjects 
def delete_deleted_aau_subject
 @aau_subject = AauSubject.find(params[:id])
 @aau_subjects = AauSubject.find(:all,:conditions => ["is_deleted = ?", false])
 @count = @aau_subjects.count
 @inc = 0
 @aau_subjects.each do |i|
  @inc = @inc + 1
  @pre = AauSubject.find(i.id)
  unless @pre.prerequisite.nil?
    @pre.prerequisite.each do |m|              
      m.to_i
      if m.to_i == @aau_subject.id
        flash[:notice]="Cant Able to Delete,Because Subject #{@pre.aau_subject_name} has Prerequiste"
        return redirect_to :action => "add_aau_subject"
      end
    end
  end
end
  if @inc ==  @count
   @aau_subject.destroy
  # @aau_subject.update_attribute(:is_deleted, true)
  flash[:notice]="#{t('deleted_successfully1')}"
  redirect_to :action => "add_aau_subject"
end 
end
# Prerequisites 
def prerequisite_subjects
  @study_plan_categories = StudyPlanCategory.find(:all)
  @aau_subjects = AauSubject.find(:all, :conditions => ["is_deleted = ?", false],:order => "aau_subject_name asc")
  if request.post? 
    @aau_subject = AauSubject.find(params[:subject][:aau_subject_id])
    @aau_subject.update_attribute(:prerequisite,params[:prerequisite])     
    flash[:notice] = "#{t('success')}"
  end
end

# Action 1 Sorting Through Study Plan And Respective Category
def list_study_plan
  @study_plan_categories = StudyPlanCategory.find(:all)
  render :update do |page|
    page.replace_html 'list-category-batch', :partial => 'list_study_category'
  end
end

# Action 2
def list_category
  @aau_subject = AauSubject.find(params[:aau_subject_id])
  render :update do |page|
    page.replace_html 'list-category-batch1', :partial => 'list_type_category'
  end
end

# Action 3
def list_subjects
  if params[:sub_category_id] == "all"
    @aau_subject = AauSubject.find(params[:aau_subject_id])
    @study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
    @subject1 = AauSubject.find_all_by_study_plan_category_id(params[:study_plan_category_id],:conditions => ["id != ? AND is_deleted = ?", @aau_subject, false])
     @subject = @subject1.reject{|x| x.prerequisite.include? @aau_subject.id.to_s unless x.prerequisite.nil?}
    @sub_category = @study_plan_category.sub_categories
    @p = []
    @sub_category.each do |i|
      @p << i.name
    end
  elsif params[:sub_category_id] == "mandatory"
    @aau_subject = AauSubject.find(params[:aau_subject_id])
    @study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
    @sub_category = @study_plan_category.sub_categories
    @p = []
    @sub_category.each do |i|
     if i.name == "Mandatory"
       @p << i.name
      sub_category_id = i.id
      @subject1 = AauSubject.find_all_by_sub_category_id_and_study_plan_category_id(sub_category_id,params[:study_plan_category_id],:conditions => ["id != ? AND is_deleted = ?", @aau_subject, false])
      @subject = @subject1.reject{|x| x.prerequisite.include? @aau_subject.id.to_s unless x.prerequisite.nil?}
    end
  end    
elsif params[:sub_category_id] == "elective"
  @aau_subject = AauSubject.find(params[:aau_subject_id])
  @study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
  @sub_category = @study_plan_category.sub_categories
  @p = []
  @sub_category.each do |i|
   if i.name == "Elective"
    sub_category_id = i.id
    @p << i.name
    @subject1 = AauSubject.find_all_by_sub_category_id_and_study_plan_category_id(sub_category_id,params[:study_plan_category_id],:conditions => ["id != ? AND is_deleted = ?", @aau_subject, false])
    @subject = @subject1.reject{|x| x.prerequisite.include? @aau_subject.id.to_s unless x.prerequisite.nil?}
  end
end
elsif params[:sub_category_id] == "select"
  @aau_subject = AauSubject.find(params[:aau_subject_id])
  @study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
end
render :update do |page|
  page.replace_html 'list-category-batch2', :partial => 'list_subject'
end
end

  def search_ajax
    if params[:option] == "active" 
      if params[:query].length>= 2
        @students = AauSubject.find(:all,:conditions => ["ltrim(aau_subject_name) LIKE ? OR ltrim(aau_subject_code) LIKE ? OR ltrim(aau_subject_name) LIKE ?
                            OR aau_subject_name = ? OR (concat(ltrim(rtrim(aau_subject_name)), \" \",ltrim(rtrim(aau_subject_name))) LIKE ? )
                              OR (concat(ltrim(rtrim(aau_subject_name)), \" \", ltrim(rtrim(aau_subject_name)), \" \",ltrim(rtrim(aau_subject_name))) LIKE ? ) ",
            "#{params[:query]}%","#{params[:query]}%","#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}%", "#{params[:query]}%" ],
          :order => "aau_subject_name asc") 
      else
        @students = AauSubject.find(:all,:conditions => ["aau_subject_code = ? " , params[:query]],:order => "aau_subject_name asc") 
      end   
      render :layout => false
    else
    if params[:query].length>= 2
        @students = AauSubject.find(:all,:conditions => ["ltrim(aau_subject_name) LIKE ? OR ltrim(aau_subject_code) LIKE ? OR ltrim(aau_subject_name) LIKE ?
                            OR aau_subject_name = ? OR (concat(ltrim(rtrim(aau_subject_name)), \" \",ltrim(rtrim(aau_subject_name))) LIKE ? )
                              OR (concat(ltrim(rtrim(aau_subject_name)), \" \", ltrim(rtrim(aau_subject_name)), \" \",ltrim(rtrim(aau_subject_name))) LIKE ? ) ",
            "#{params[:query]}%","#{params[:query]}%","#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}%", "#{params[:query]}%" ],
          :order => "aau_subject_name asc") 
      else
        @students = AauSubject.find(:all,:conditions => ["aau_subject_code = ? " , params[:query]],:order => "aau_subject_name asc") 
      end
      render :partial => "search_ajax"
    end
  end



end
