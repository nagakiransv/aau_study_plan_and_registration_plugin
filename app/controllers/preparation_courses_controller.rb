class PreparationCoursesController < ApplicationController
	def index
		@preparation_courses = PreparationCourse.find(:all,:order => "name asc")
	end

	def new
		begin
		@preparation_course = PreparationCourse.new(params[:preparation_course])
		if request.post? and @preparation_course.save
			flash[:notice] =  t('preparation_course_added_success')
			redirect_to :action => "index"
		end
		rescue Exception => e
        Rails.logger.info "Exception in PreparationCoursesController, new action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :aau_home, :action => :index
      end
	end

	def edit
		begin
		@preparation_course = PreparationCourse.find(params[:id])
		if request.post? and @preparation_course.update_attributes(params[:preparation_course])
			flash[:notice] = "#{t('preparation_course_update_success')}"
			redirect_to :action => "index"
		end
		rescue Exception => e
        Rails.logger.info "Exception in PreparationCoursesController, edit action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :aau_home, :action => :index
      end
	end

	def destroy
		@preparation_course = PreparationCourse.find(params[:id])
		@preparation_course.destroy
		flash[:notice]="#{t('deleted_successfully')}"
		redirect_to :action => "index"
	end

	def assign_preparation_courses
		begin
		@preparation_course = PreparationCourse.find(params[:id])
		@preparation_courses_student = PreparationCoursesStudent.find_by_preparation_course_id(params[:id])
		@batches = Batch.active
		if  request.post?
			@preparation_courses_student = PreparationCoursesStudent.find_by_preparation_course_id_and_batch_id(params[:id],params[:batch][:id])
			unless @preparation_courses_student.nil?
				@preparation_courses_student.update_attributes(:preparation_course_id => params[:preparation_courses_student][:preparation_course_id],:student_id => params[:student_id],:batch_id => params[:batch][:id])
				flash[:notice] = "#{t('updated_students_successfully')}"
				redirect_to :action => "assign_preparation_courses"
			else
				@preparation_courses_student = PreparationCoursesStudent.new(:preparation_course_id => params[:preparation_courses_student][:preparation_course_id],:student_id => params[:student_id],:batch_id => params[:batch][:id])
				@preparation_courses_student.save 
				flash[:notice] = "#{t('students_assigned_successfully')}"
				redirect_to :action => "assign_preparation_courses"
			end
		end
		rescue Exception => e
        Rails.logger.info "Exception in PreparationCoursesController, assign_preparation_courses action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :aau_home, :action => :index
      end
	end

	# def edit_assign_preparation_courses
	# 	begin
	# 	@preparation_course = PreparationCourse.find(params[:id])
	# 	@batches = Batch.active
	# 	@preparation_courses_student = PreparationCoursesStudent.find_by_preparation_course_id(params[:id])
	# 	if  request.post?
	# 		@preparation_courses_student = PreparationCoursesStudent.find_by_preparation_course_id_and_batch_id(params[:id],params[:batch][:id])
	# 		unless @preparation_courses_student.nil?
	# 			@preparation_courses_student.update_attributes(:preparation_course_id => params[:preparation_courses_student][:preparation_course_id],:student_id => params[:student_id],:batch_id => params[:batch][:id])
	# 			flash[:notice] = "#{t('updated_students_successfully')}"
	# 			redirect_to :action => "edit_assign_preparation_courses"
	# 		else
	# 			@preparation_courses_student = PreparationCoursesStudent.new(:preparation_course_id => params[:preparation_courses_student][:preparation_course_id],:student_id => params[:student_id],:batch_id => params[:batch][:id])
	# 			@preparation_courses_student.save 
	# 			flash[:notice] = "#{t('students_assigned_successfully')}"
	# 			redirect_to :action => "edit_assign_preparation_courses"
	# 		end
	# 	end
	# 	rescue Exception => e
 #        Rails.logger.info "Exception in PreparationCoursesController, edit_assign_preparation_courses action"
 #        Rails.logger.info e
 #        flash[:notice] = "Sorry, something went wrong. Please inform administration"
 #        redirect_to :controller => :aau_home, :action => :index
 #      end
	# end

	def list_students
		@preparation_courses_student = PreparationCoursesStudent.find_by_preparation_course_id_and_batch_id(params[:id],params[:batch_id])
			if params[:batch_id].present?
				@batch=Batch.find(params[:batch_id])
				@students = Student.active.find_all_by_batch_id(@batch.id)
			end
		render :update do |page|
			page.replace_html 'list-batch-students', :partial => 'list_students'
		end
	end


end
