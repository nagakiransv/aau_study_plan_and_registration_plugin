class StudyPlansController < ApplicationController
	def index
		@study_plans = StudyPlan.find(:all,:order => "plan_issue asc")
	end

  def new
    @study_plan = StudyPlan.new(params[:study_plan])
    @study_plan_categories = StudyPlanCategory.all
    if request.post? and @study_plan.save
      category_and_subcategory
      catch :out do
        unless params[:subject_ids].nil?
          validate_credit_hours
          params[:subject_ids].each do |s|
            @aau_subjects_study_plans = AauSubjectsStudyPlan.new(:aau_subject_id => s,:study_plan_id => @study_plan.id)
            @aau_subjects_study_plans.save
          end 
        end
      end
      if @within_limit == 1
        flash[:notice] =  t('study_plan_added_success')
      elsif @within_limit == 0
        flash[:notice] = "#{t('subjects_are_not_selected_because_credit_hour_limit_exceeded_so_uncheck_and_update_again')}"
      end
      redirect_to :action => "index"
    end
  end

	def edit
		@study_plan = StudyPlan.find(params[:id])
		@study_plan_categories = StudyPlanCategory.all
		@aau_subjects_study_plan = AauSubjectsStudyPlan.find_all_by_study_plan_id(@study_plan.id)
		sort_mandatory_and_elective
		if request.post? and @study_plan.update_attributes(params[:study_plan])
			category_and_subcategory
			unless @aau_subjects_study_plan.nil? and  params[:cat][:study_plan_category_id].nil? and  params[:sub][:sub_category_id].nil?
				subject_ids =  @aau_subjects_study_plan.map {|p| p.aau_subject_id}
				a_subjects = []
				unless subject_ids.nil?
					subject_ids.each do |subjects|
						as = AauSubject.find_by_id_and_study_plan_category_id_and_sub_category_id(subjects,params[:cat][:study_plan_category_id],params[:sub][:sub_category_id])
						unless as.nil?
							a_subjects << as.id
						end
					end
				end
			end
			unless params[:subject_ids].nil?
        unchecked = a_subjects - params[:subject_ids].map(&:to_i)
      else
       unchecked = a_subjects - params[:subject_ids].to_a
      end
      unless unchecked.empty?
        unchecked.each do |subject|
          @aau_subject_plans = AauSubjectsStudyPlan.find_by_aau_subject_id_and_study_plan_id(subject,@study_plan.id)
          if (@studybatch = BatchesStudyPlan.find_by_study_plan_id(@aau_subject_plans.study_plan_id))
            @aau_fedena = AauSubjectsFedenaSubject.find_by_aau_subject_id_and_batch_id(subject,@studybatch.batch_id)
            @fedena_subject = Subject.find_by_id(@aau_fedena.fedena_subject_id)
            @fedena_subject.destroy
            @aau_fedena.destroy
          end
					@aau_subject_plans.destroy
				end
			end
      catch :out do
  			unless params[:subject_ids].nil?
          validate_credit_hours
          params[:subject_ids].each do |@sub_id|
          	@aau_subjects_study_plans = AauSubjectsStudyPlan.find_by_aau_subject_id_and_study_plan_id(@sub_id,@study_plan.id)
          	if @aau_subjects_study_plans.nil?
              @aau_subjects_study_plans = AauSubjectsStudyPlan.new(:aau_subject_id => @sub_id,:study_plan_id => @study_plan.id)
              @aau_subjects_study_plans.save
              if (@studybatch = BatchesStudyPlan.find_by_study_plan_id(@study_plan.id))
                @aau_sub = AauSubject.find(@aau_subjects_study_plans.aau_subject_id)
                if @aau_sub.sub_category.name == "Mandatory"
                  @fedena_edit_normal_subject = Subject.new(:batch_id => @studybatch.batch_id,:name => @aau_sub.aau_subject_name, :code => @aau_sub.aau_subject_code,:max_weekly_classes => @aau_sub.max_weekly_classes,:no_exams => @aau_sub.no_exam,:is_deleted => @aau_sub.is_deleted,:credit_hours => @aau_sub.aau_credit_hours )
                  @fedena_edit_normal_subject.save
                  @aau_subjects_edit_normal_fedena_subjects = AauSubjectsFedenaSubject.new(:batch_id => @studybatch.batch_id,:aau_subject_id => @aau_sub.id,:fedena_subject_id => @fedena_edit_normal_subject.id)
                  @aau_subjects_edit_normal_fedena_subjects.save
                else
                  batchs = Batch.find(@studybatch.batch_id)
                  if batchs.elective_groups.empty?
                    @elective_group = ElectiveGroup.new(:batch_id => @studybatch.batch_id, :name => "Optional")
                    @elective_group.save
                  end
                  @fedena_edit_elective_subject = Subject.new(:batch_id => @studybatch.batch_id,:name => @aau_sub.aau_subject_name, :code => @aau_sub.aau_subject_code,:max_weekly_classes => @aau_sub.max_weekly_classes,:no_exams => @aau_sub.no_exam,:is_deleted => @aau_sub.is_deleted,:credit_hours => @aau_sub.aau_credit_hours,:elective_group_id => batchs.elective_groups.find_by_name("Optional").id )
                  @fedena_edit_elective_subject.save
                  @aau_subjects_edit_elective_fedena_subjects = AauSubjectsFedenaSubject.new(:batch_id => @studybatch.batch_id,:aau_subject_id => @aau_sub.id,:fedena_subject_id => @fedena_edit_elective_subject.id, :elective_group_id => @fedena_edit_elective_subject.elective_group_id)
                  @aau_subjects_edit_elective_fedena_subjects.save
                end
              end
           	end
          end 
       	end
      end
     	update_equivalence
      flash[:notice] =  t('study_plan_update_success')
      if @within_limit == 1
        flash[:notice] =  t('study_plan_update_success')
      elsif @within_limit == 0
        flash[:notice] = "#{t('subjects_are_not_selected_because_credit_hour_limit_exceeded_so_uncheck_and_update_again')}"
      end     	
			redirect_to :action => "edit"
		end
	end

	def show
		@study_plan = StudyPlan.find(params[:id])
		@aau_subjects_study_plan = AauSubjectsStudyPlan.find_all_by_study_plan_id(@study_plan.id)
		sort_mandatory_and_elective
		if request.post?
			update_equivalence
			redirect_to :action => "show"
		end
	end

	def destroy
		@study_plan = StudyPlan.find(params[:id])
		@study_plan.destroy
		flash[:notice]="#{t('deleted_successfully')}"
		redirect_to :action => "index"
	end

	def list_sub_category
		@study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
		@sub_categories = @study_plan_category.sub_categories
		render :update do |page|
 		 page.replace_html 'list-sub-category', :partial => 'list_sub_category'
		end
	end

	def list_subjects
		@study_plan_id = params[:id]
  	@aau_subjects = AauSubject.find_all_by_study_plan_category_id_and_sub_category_id(params[:study_plan_category_id],params[:sub_category_id],:conditions => ["is_deleted = ?", false])
  	@sub_cat = SubCategory.find(params[:sub_category_id])
    render :update do |page|
   		page.replace_html 'list-subjects-to-select', :partial => 'list_subjects'
   		page.replace_html 'submit-button', :partial => 'submit_button'
 		end
	end  

def assign_study_plan
    @study_plan = StudyPlan.find(params[:id])
    @courses = Course.active
    @batch_study = BatchesStudyPlan.find_by_study_plan_id(@study_plan.id)
    unless @batch_study.nil?
      @batch = Batch.find(@batch_study.batch_id)
    end
    if request.post?
      unless @batch.nil?
        unless params[:batch_ids].nil?
          unchecked_batch = [@batch.id] - params[:batch_ids].map(&:to_i)
        else
          unchecked_batch = [@batch.id] - params[:batch_ids].to_a
        end
        unless unchecked_batch.empty?
          @subj = Subject.find_all_by_batch_id(unchecked_batch)
          unless @subj.empty?
            @subj.each do |i|
              i.destroy
              @aau_sub_fedena_sub = AauSubjectsFedenaSubject.find_by_fedena_subject_id_and_batch_id(i,unchecked_batch)
              unless @aau_sub_fedena_sub.nil?
                @aau_sub_fedena_sub.destroy
              end
            end
          end
          @batch_study.destroy
        end
      end
      unless params[:batch_ids].nil?
        params[:batch_ids].each do |b|
            @batch_study = BatchesStudyPlan.find_by_batch_id_and_study_plan_id(b,@study_plan.id)
            if @batch_study.nil?
              @batches_study_plans = BatchesStudyPlan.new(:batch_id => b,:study_plan_id => @study_plan.id)
              @batches_study_plans.save
              @aau_subjects_study_plan = AauSubjectsStudyPlan.find_all_by_study_plan_id(@study_plan.id)
              sort_mandatory_and_elective
              unless @mandatory.empty?
                @mandatory.each do |l|
                  aau_fedena_subjects = AauSubjectsFedenaSubject.find_by_aau_subject_id_and_batch_id(l.id,b)
                  if aau_fedena_subjects.nil?
                    @fedena_normal_subject = Subject.new(:batch_id => b,:name => l.aau_subject_name, :code => l.aau_subject_code,:max_weekly_classes => l.max_weekly_classes,:no_exams => l.no_exam,:is_deleted => l.is_deleted,:credit_hours => l.aau_credit_hours )
                    @fedena_normal_subject.save
                    @aau_subjects_normal_fedena_subjects = AauSubjectsFedenaSubject.new(:batch_id => b,:aau_subject_id => l.id,:fedena_subject_id => @fedena_normal_subject.id)
                    @aau_subjects_normal_fedena_subjects.save
                  end
                end
              end
              unless @elective.empty?
                @b = b
                @batchs = Batch.find(b)
                if @batchs.elective_groups.empty?
                  @elective_group = ElectiveGroup.new(:batch_id => b, :name => "Optional")
                  @elective_group.save
                  elective_subjects_save
                else
                  elective_subjects_save
                end
              end
            end
        end 
      end
      redirect_to :action => "assign_study_plan"
    end
   end

   def list_batches
    @study_plan = StudyPlan.find(params[:id])
    @course = Course.find(params[:course_id])
    @batches = @course.batches.active
    @a = BatchesStudyPlan.all.map{|a| a.batch_id}
    @b = @batches.map{|a| a.id}
    @c = @b - @a
    @batches = []
    unless @c.nil?
      @c.each do |i|
        @batches << Batch.find(i)
      end
    end
    @d = BatchesStudyPlan.find_by_study_plan_id(@study_plan.id)
    unless @d.nil?
      @batch = Batch.find_by_id_and_course_id(@d.batch_id,params[:course_id])
    end
    render :update do |page|
      page.replace_html 'list-batch', :partial => 'list_batches'
    end
   end
   
  def plan_prerequisite
    @study_plan_categories = StudyPlanCategory.find(:all)
    @aau_subject = AauSubject.find(params[:id])
    @study_plan = StudyPlan.find(params[:study_plan_id])
    if request.post?
      @study_plan_subject = AauSubjectsStudyPlan.find_by_aau_subject_id_and_study_plan_id(params[:id],params[:study_plan_id])
      @a = (AauSubjectsStudyPlan.find_by_aau_subject_id_and_study_plan_id(params[:id],params[:study_plan_id])).prerequisite
      @b = []
      @e = []
      unless @a.nil? or @a.empty?
        @b = @a.map(&:to_i)
        @c = @b.select {|p| (AauSubject.find(p).study_plan_category_id == params[:study_sub][:study_plan_category_id].to_i and AauSubject.find(p).sub_category_id == params[:study_sub][:sub_category_id].to_i)}
        unless params[:prerequisite].nil?
          @d = @c - params[:prerequisite].map(&:to_i)
        else
          @d = @c - params[:prerequisite].to_a
        end
        @e = @b - @d
      end
      unless params[:prerequisite].nil?
        @f = (@e.concat(params[:prerequisite].map(&:to_i))).uniq
      else
        @f = @e
      end
      @study_plan_subject.update_attribute(:prerequisite,@f)  
      flash[:notice] = "#{t('success')}"
    end
   end

    def list_plan_categories
      @study_plan = StudyPlan.find(params[:study_plan_id])
       @aau_subject = AauSubject.find(params[:id])
       @study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
       @sub_category = @study_plan_category.sub_categories
         render :update do |page|
      page.replace_html 'list-category-batch1', :partial => 'list_plan_categories'
    end
      
   end


   def list_plan_subjects
      @study_plan = StudyPlan.find(params[:study_plan_id])
      @aau_subject = AauSubject.find(params[:aau_subject_id])
      @select_subject = AauSubjectsStudyPlan.find_by_aau_subject_id_and_study_plan_id(params[:aau_subject_id],params[:study_plan_id])
      @study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
      @subject = AauSubject.find_all_by_study_plan_category_id_and_sub_category_id(params[:study_plan_category_id],params[:sub_category_id],:conditions => ["id != ? AND is_deleted = ?", @aau_subject, false])
    render :update do |page|
      page.replace_html 'list-category-batch2', :partial => 'list_plan_subjects'
    end
   end

  protected
  def sort_mandatory_and_elective
    unless @aau_subjects_study_plan.nil?
      @aau_subject_ids =  @aau_subjects_study_plan.map {|p| p.aau_subject_id}
      @subjects = @aau_subject_ids.map {|p| AauSubject.find(p)}
      @mandatory_total = 0
      @elective_total = 0
      @mandatory = @subjects.select {|p| p.sub_category.name == "Mandatory"}
      @elective = @subjects.reject {|p| p.sub_category.name == "Mandatory"}
      unless @mandatory.empty?
        a = @mandatory.map {|p| p.aau_credit_hours.to_i}
        @mandatory_total = a.inject{|sum,x| sum + x }
      end
      unless @elective.empty?
        b = @elective.map {|p| p.aau_credit_hours.to_i}
        @elective_total = b.inject{|sum,x| sum + x }
      end
    else
      @subjects = []
    end
  end

  def category_and_subcategory
    unless params[:cat][:study_plan_category_id].nil?
      @categories_study_plan = StudyPlanCategoriesStudyPlan.find_by_study_plan_category_id_and_study_plan_id(params[:cat][:study_plan_category_id],@study_plan.id)
      if @categories_study_plan.nil?
        @categories_study_plan = StudyPlanCategoriesStudyPlan.new(:study_plan_category_id => params[:cat][:study_plan_category_id],:study_plan_id => @study_plan.id)
        @categories_study_plan.save
      end
    end
    unless params[:sub][:sub_category_id].nil?
      @study_plan_sub_category = StudyPlansSubCategory.find_by_sub_category_id_and_study_plan_id(params[:sub][:sub_category_id],@study_plan.id)
      if @study_plan_sub_category.nil?
        @study_plan_sub_category = StudyPlansSubCategory.new(:sub_category_id => params[:sub][:sub_category_id],:study_plan_id => @study_plan.id)
        @study_plan_sub_category.save
      end
    end
  end

  def update_equivalence
    unless @aau_subjects_study_plan.empty?
      @aau_subjects_study_plan.each do |equi|
        equi.update_attribute(:equivalence,false)
      end
    end
    unless params[:equivalence_ids].nil?
      params[:equivalence_ids].each do |sub_id|
        @aau_subject_study = AauSubjectsStudyPlan.find_by_study_plan_id_and_aau_subject_id(@study_plan.id,sub_id)
        unless @aau_subject_study.nil?
          @aau_subject_study.update_attribute(:equivalence,true)
        end
      end
    end
    flash[:notice] =  t('equivalence_update_success')
  end

  def elective_subjects_save
    @elective.each do |l|
      aau_fedena_subjects = AauSubjectsFedenaSubject.find_by_aau_subject_id_and_batch_id(l.id,@b)
      if aau_fedena_subjects.nil?
        @fedena_elective_subject = Subject.new(:batch_id => @b,:name => l.aau_subject_name, :code => l.aau_subject_code,:max_weekly_classes => l.max_weekly_classes,:no_exams => l.no_exam,:is_deleted => l.is_deleted,:credit_hours => l.aau_credit_hours,:elective_group_id => @batchs.elective_groups.find_by_name("Optional").id )
        @fedena_elective_subject.save
        @aau_subjects_elective_fedena_subjects = AauSubjectsFedenaSubject.new(:batch_id => @b,:aau_subject_id => l.id,:fedena_subject_id => @fedena_elective_subject.id, :elective_group_id => @fedena_elective_subject.elective_group_id)
        @aau_subjects_elective_fedena_subjects.save
      end
    end
  end

  def validate_credit_hours
    tsum = 0
    @aau_subjects_study_plan = AauSubjectsStudyPlan.find_all_by_study_plan_id(@study_plan.id)
    unless @aau_subjects_study_plan.empty?
      @s = @aau_subjects_study_plan.map {|p| AauSubject.find(p.aau_subject_id)}
      @a = []
      @s.each do |a|
        if a.sub_category.name == "Mandatory"
          @a << a.aau_credit_hours.to_i
        end
      end
      tsum = @a.inject{|sum,x| sum + x }
      if tsum.nil?
        tsum = 0
      end            
    end
    @new_cr_hr = 0
    asub = params[:subject_ids].select {|p| AauSubject.find(p).sub_category.name == "Mandatory"}
    unless asub.empty?
      aausub = asub.reject {|p| AauSubject.find(p).aau_subjects_study_plans.select {|p| p.study_plan_id == @study_plan.id} != []}
      unless asub.empty?
        @total_cr_hr = aausub.map {|p| AauSubject.find(p).aau_credit_hours.to_i}
        @new_cr_hr = @total_cr_hr.inject{|sum,x| sum + x }
      end
    end
    if !(tsum+@new_cr_hr <= @study_plan.credit_hours)
      @within_limit = 0
      throw :out
    else
      @within_limit = 1
    end
  end

end

################################################

    # def validate_credit_hours
    # sum = 0
    # @aau_subjects_study_plan = AauSubjectsStudyPlan.find_all_by_study_plan_id(@study_plan.id)
    # unless @aau_subjects_study_plan.empty?
    #   @s = @aau_subjects_study_plan.map {|p| AauSubject.find(p.aau_subject_id)}
    #   @a = []
    #   @s.each do |a|
    #     if a.sub_category.name == "Mandatory"
    #       @a << a.aau_credit_hours.to_i
    #     end
    #   end
    #   sum = @a.inject{|sum,x| sum + x }
    #   if sum.nil?
    #     sum = 0
    #   end            
    # end
    # subject_credit_hour = AauSubject.find(@sub_id)
    # if subject_credit_hour.sub_category.name == "Mandatory"
    #   if !(sum+subject_credit_hour.aau_credit_hours.to_i <= @study_plan.credit_hours)
    #     flash[:notice] = "#{subject_credit_hour.aau_subject_name}" + " " + "#{t('is_not_selected_because_credit_hour_limit_exceeded_so_uncheck_and_update_again')}"
    #     @check = 0
    #     throw :out
    #   else
    #     @aau_subjects_study_plans = AauSubjectsStudyPlan.new(:aau_subject_id => @sub_id,:study_plan_id => @study_plan.id)
    #     @aau_subjects_study_plans.save
    #     @check = 1
    #   end
    # else
    #   @aau_subjects_study_plans = AauSubjectsStudyPlan.new(:aau_subject_id => @sub_id,:study_plan_id => @study_plan.id)
    #   @aau_subjects_study_plans.save
    #   @check = 1
    # end
    # end

    
  # def new
  #   @study_plan = StudyPlan.new(params[:study_plan])
  #   @study_plan_categories = StudyPlanCategory.all
  #   if request.post? and @study_plan.save
  #     category_and_subcategory
 #      catch :out do
 #        unless params[:subject_ids].nil?
 #          params[:subject_ids].each do |@sub_id|
 #            validate_credit_hours
 #          end 
 #        end
 #      end
 #        flash[:notice] =  t('study_plan_added_success')
  #     redirect_to :action => "index"
  #   end
  # end

#####################################################################################################
# the below actions are needed only when they require to assign same study plan to multiple Batches #
#####################################################################################################

# def assign_study_plan
#     @study_plan = StudyPlan.find(params[:id])
#     @courses = Course.active
#     d = BatchesStudyPlan.find_all_by_study_plan_id(@study_plan.id)
#     @e = d.map{|a| a.batch_id}
#     @batch = []
#       unless @e.nil?
#         @e.each do |i|
#           @batch << Batch.find(i)
#         end
#       end
#     if request.post?
#       unless @e.nil?
#         @batch_id = []
#         @e.each do |i|
#           ab = Batch.find_by_id_and_course_id(i,params[:bat][:course_id])
#           unless ab.nil?
#             @batch_id << ab.id
#           end
#         end
#       end
#       unless params[:batch_ids].nil?
#         unchecked = @batch_id.to_a - params[:batch_ids].map(&:to_i)
#       else
#         unchecked = @batch_id.to_a - params[:batch_ids].to_a
#       end
#         unless unchecked.empty?
#           unchecked.each do |batch|
#             @batch_study = BatchesStudyPlan.find_by_batch_id_and_study_plan_id(batch,@study_plan.id)
#             @subj = Subject.find_all_by_batch_id(batch)
#             unless @subj.empty?
#               @subj.each do |i|
#                 i.destroy
#                 @aau_sub_fedena_sub = AauSubjectsFedenaSubject.find_by_fedena_subject_id_and_batch_id(i,batch)
#                 unless @aau_sub_fedena_sub.nil?
#                   @aau_sub_fedena_sub.destroy
#                 end
#               end
#             end
#             @batch_study.destroy
#           end
#         end
#       unless params[:batch_ids].nil?
#         params[:batch_ids].each do |b|
#             @batch_study = BatchesStudyPlan.find_by_batch_id_and_study_plan_id(b,@study_plan.id)
#             if @batch_study.nil?
#               @batches_study_plans = BatchesStudyPlan.new(:batch_id => b,:study_plan_id => @study_plan.id)
#               @batches_study_plans.save
#               @aau_subjects_study_plan = AauSubjectsStudyPlan.find_all_by_study_plan_id(@study_plan.id)
#               sort_mandatory_and_elective
#               unless @mandatory.empty?
#                 @mandatory.each do |l|
#                   aau_fedena_subjects = AauSubjectsFedenaSubject.find_by_aau_subject_id_and_batch_id(l.id,b)
#                   if aau_fedena_subjects.nil?
#                     @fedena_normal_subject = Subject.new(:batch_id => b,:name => l.aau_subject_name, :code => l.aau_subject_code,:max_weekly_classes => l.max_weekly_classes,:no_exams => l.no_exam,:is_deleted => l.is_deleted,:credit_hours => l.aau_credit_hours )
#                     @fedena_normal_subject.save
#                     @aau_subjects_normal_fedena_subjects = AauSubjectsFedenaSubject.new(:batch_id => b,:aau_subject_id => l.id,:fedena_subject_id => @fedena_normal_subject.id)
#                     @aau_subjects_normal_fedena_subjects.save
#                   end
#                 end
#               end
#               unless @elective.empty?
#                 @b = b
#                 @batchs = Batch.find(b)
#                 if @batchs.elective_groups.empty?
#                   @elective_group = ElectiveGroup.new(:batch_id => b, :name => "Optional")
#                   @elective_group.save
#                   elective_subjects_save
#                 else
#                   elective_subjects_save
#                 end
#               end
#             end
#         end 
#       end
#       redirect_to :action => "assign_study_plan"
#     end
#    end

   # def list_batches
   #  @study_plan = StudyPlan.find(params[:id])
   #  @course = Course.find(params[:course_id])
   #  @batches = @course.batches.active
   #  @a = BatchesStudyPlan.all.map{|a| a.batch_id}
   #  @b = @batches.map{|a| a.id}
   #  @c = @b - @a
   #  @batches = []
   #  unless @c.nil?
   #    @c.each do |i|
   #      @batches << Batch.find(i)
   #    end
   #  end
   #  d = BatchesStudyPlan.find_all_by_study_plan_id(@study_plan.id)
   #  unless d.nil? and params[:course_id].nil?
   #    @e = d.map{|a| a.batch_id}
   #    @batch = []
   #    unless @e.nil?
   #      @e.each do |i|
   #        ab = Batch.find_by_id_and_course_id(i,params[:course_id])
   #        unless ab.nil?
   #          @batch << ab
   #        end
   #      end
   #    end
   #  end
   #  render :update do |page|
   #    page.replace_html 'list-batch', :partial => 'list_batches'
   #  end
   # end