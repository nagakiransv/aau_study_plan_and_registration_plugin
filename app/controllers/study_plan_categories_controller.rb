class StudyPlanCategoriesController < ApplicationController


 def add_study_plan_category
   flash[:clear]
   @study_plan_categories =StudyPlanCategory.find(:all,:order => "name asc")
   @study_plan_category = StudyPlanCategory.new(params[:study_plan_category])
   if request.post? and  @study_plan_category.save
     @sub_categories =SubCategory.find(:all,:order => "name asc")
     @sub_category_type = ["Mandatory"]
     @sub_category_type.each do |type|
      if type == "Mandatory"
        @sub_category =SubCategory.new( :study_plan_category_id => @study_plan_category.id,:name =>type,:code =>@study_plan_category.code+"m1",:description =>"mandatory field",:credit_hours=>"")
        @sub_category.save
      end
    end
    flash[:notice] =  t('flash9')
    redirect_to :action => "show" , :id => @study_plan_category.id
  end
end

def edit_study_plan_category
 flash[:clear]
 @study_plan_categories =StudyPlanCategory.find(:all)
 @study_plan_category = StudyPlanCategory.find(params[:id])
 @sub_category = SubCategory.find_all_by_study_plan_category_id(params[:id])
 if request.post? and @study_plan_category.update_attributes(params[:study_plan_category])
  @sub_category.each do |sub|
    if sub.name == "Mandatory"
      sub.update_attributes(:code =>@study_plan_category.code+"m1")
    end
  end
  flash[:notice] = "#{t('flash2')}"
  redirect_to :action => "show" , :id => @study_plan_category.id
end
end

def delete_study_plan_category
 flash[:clear]
 @study_plan_categories=StudyPlanCategory.find(:all, :order=>"name Asc")
 @study_plan_category = StudyPlanCategory.find(params[:id])
 @study_plan_category.destroy
 flash[:notice]="#{t('flash3')}"
 redirect_to :action => "add_study_plan_category"
end

def show
 @study_plan_category = StudyPlanCategory.find(params[:id])
 @sub_category = @study_plan_category.sub_categories
 if request.post?
  params[:assign_study_plan].each_pair do |i,details|
    @sub_cat = SubCategory.find i
    if @sub_cat .update_attributes(details)
     flash[:notice]="Update Sucessfully"
   else
    flash[:notice]="Error Updating Credit Hours"
  end
end
end
end

def add_electives
  @study_plan_cat = StudyPlanCategory.find(params[:id])
  @sub_category = @study_plan_cat.sub_categories.new(params[:study_plan_category])
  if request.post? 
   if  params[:study_plan_category][:name].downcase != "mandatory" 
     @sub_category.save
     flash[:notice] = "#{t('flash2')}"
     redirect_to :action => "show" ,:id => @sub_category.study_plan_category_id
   else
    flash[:warn_notice] = "#{t('cant_give_mandatory')}"
    redirect_to :action => "add_electives" ,:id => @study_plan_cat.id
  end

end

end


def edit_electives
  @sub_cat = SubCategory.find(params[:id])
  @study = StudyPlanCategory.find(@sub_cat.study_plan_category_id)
  if request.post?
    if  params[:study_plan_category][:name].downcase != "mandatory"
      @sub_cat.update_attributes(params[:study_plan_category])   
      flash[:notice] = "#{t('success')}"
      redirect_to :action => "show",:id => @sub_cat.study_plan_category_id
    else
      flash[:warn_notice] = "#{t('cant_give_mandatory')}"
      redirect_to :action => "edit_electives" ,:id => @sub_cat.id
    end
  end
end


end

