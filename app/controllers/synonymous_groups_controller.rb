class SynonymousGroupsController < ApplicationController

	def new_synonymous_group
    @synonymous_groups = SynonymousGroup.find(:all,:order => "name asc")
    @synonymous_group = SynonymousGroup.new(params[:synonymous_group])
    if request.post? and @synonymous_group.save
      flash[:notice] =  t('add_synonymous_group_success')
      redirect_to :action => "new_synonymous_group" 
    end
  end

  def edit_synonymous_group
    @synonymous_groups =SynonymousGroup.find(:all)
    @synonymous_group = SynonymousGroup.find(params[:id])
    if request.post? and @synonymous_group.update_attributes(params[:synonymous_group])
      flash[:notice] = "#{t('updated_successfully')}"
      redirect_to :action => "new_synonymous_group"
    end
  end

  def delete_synonymous_group
    @synonymous_group = SynonymousGroup.find(params[:id])
    @groups = AauSubject.find_all_by_synonymous_group_id(params[:id],:conditions => ["is_deleted = ?", false])
    @groups.each do |as|
      as.update_attributes(:synonymous_group_id => params[:ni])
    end
    @synonymous_group.destroy
    flash[:notice]="#{t('deleted_successfully')}"
    redirect_to :action => "new_synonymous_group"
  end

  def show_subjects
    @study_plan_categories = StudyPlanCategory.find(:all)
    @synonymous_group = SynonymousGroup.find(params[:id])
    @subjects = AauSubject.find_all_by_synonymous_group_id(params[:id],:conditions => ["is_deleted = ?", false])
    @synonymous_courses = AauSubject.find_all_by_synonymous_group_id()
    @p = [] 
     @p1 = [] 
     @subjects.each do |c|
     @s = SubCategory.find(c.sub_category_id) 
   if @s.name == "Mandatory" 
    @m = c.aau_subject_name 
     @p << @m
  else
    @g =  c.aau_subject_name 
     @p1 << @g
    end 
    if request.post?
      synonymous = nil
       @aau_sub = AauSubject.find_all_by_study_plan_category_id_and_sub_category_id_and_synonymous_group_id(params[:aau_subject][:study_plan_category_id],params[:aau_subject][:sub_category_id],params[:id],:conditions => ["is_deleted = ?", false])
      unless @aau_sub.nil?
        @aau_sub.each do |as|
          as.update_attributes(:synonymous_group_id => synonymous)
        end
      end
      unless params[:subject_ids].nil?
        params[:subject_ids].each do |s|
         @aau_subject = AauSubject.find(s)
         @aau_subject.update_attributes(:synonymous_group_id => params[:id])
       end 
     end 
     unless params[:unassigned_subject_ids].nil?
      params[:unassigned_subject_ids].each do |u|
        @uaau_subject = AauSubject.find(u)
        @uaau_subject.update_attributes(:synonymous_group_id => params[:id])
      end
    end
    redirect_to :action => "show_subjects"
  end
end

# Action 2
def list_plan_category
 @synonymous_group = SynonymousGroup.find(params[:synonymous_group_id])
 @study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
 @sub_category = @study_plan_category.sub_categories
 render :update do |page|
  page.replace_html 'list-category-batch1', :partial => 'list_sub_category'
end
end

def list_category
  @synonymous_group = SynonymousGroup.find(params[:synonymous_group_id])
  @study_plan_category = StudyPlanCategory.find(params[:study_plan_category_id])
  @sub_category = SubCategory.find(params[:sub_category_id])
  @subjects = AauSubject.find_all_by_synonymous_group_id_and_study_plan_category_id_and_sub_category_id(params[:synonymous_group_id],params[:study_plan_category_id],params[:sub_category_id],:conditions => ["is_deleted = ?", false])
  synonymous = nil
  @synonymous_courses = AauSubject.find_all_by_synonymous_group_id_and_study_plan_category_id_and_sub_category_id(synonymous,params[:study_plan_category_id],params[:sub_category_id],:conditions => ["is_deleted = ?", false])
  render :update do |page|
   page.replace_html 'list-category-batch2', :partial => 'list_synonymous_group'
 end
end

end
