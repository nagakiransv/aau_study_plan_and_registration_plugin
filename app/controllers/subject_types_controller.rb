class SubjectTypesController < ApplicationController

	def add_subject_type
    @subject_types =SubjectType.find(:all,:order => "name asc")
    @subject_type = SubjectType.new(params[:subject_type])
    if request.post? and @subject_type.save
      flash[:notice] =  t('add_subject_success')
      redirect_to :action => "add_subject_type" 
    end
  end
  def edit_subject_type
    @subject_type = SubjectType.find(params[:id])
    if request.post? and @subject_type.update_attributes(params[:subject_type])
      flash[:notice] = "#{t('success')}"
      redirect_to :action => "add_subject_type"
    end
  end

   def delete_subject_type   
       @subject_type = SubjectType.find(params[:id])
         @subject_type.destroy
       flash[:notice]="#{t('deleted_successfully')}"
        redirect_to :action => "add_subject_type"
  end

end